
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Canary is a test setup for shipping and displaying Stackwave logs in a local users environment.

## Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainer](#maintainer)


## Background

Canary is an implementation of the [Elastic Stack](https://www.elastic.co/) using:

- [Filebeat](https://www.elastic.co/beats/filebeat)
- [Logstash](https://www.elastic.co/logstash/)
- [Elasticsearch](https://www.elastic.co/elasticsearch/)
- [Kibana](https://www.elastic.co/kibana/)

## Install

To install, you need to have [Docker](https://www.docker.com/) installed.

1. Setup host file. You can skip this step by renaming everything to `127.0.0.1` in the config files.
    * Windows - C:\Windows\System32\drivers\etc\hosts
    * Mac/*nix - /etc/hosts
```
127.0.0.1 es01
127.0.0.1 kb01
127.0.0.1 ls01
```

2. Setup and deploy Elasticsearch and Kibana. These are the dependencies of our system.
```
make deps
```
3. Configure Elasticsearch and Kibana. This will load index patterns, log pipelines, and dashboards. This requires Elasticsearch and Kibana to be running, which may take a minute from step 1.
```
make setup
```
4. Setup and deploy Logstash and Filebeats. Additional [Beats](https://www.elastic.co/beats/) will be added to this step.
```
make services
```

## Usage

Kibana will be located at [http://kb01:5601]()

## Maintainer
- [Mike Angellotti](mailto:michael.angellotti@stackwave.com)
