.PHONY: deps services stop_all stop_deps stop_services

cwd  := $(shell pwd)
eshost := 'es01:9200'

deps:
	@echo "Starting elasticsearch and kibana"
	docker compose -f docker-compose.deps.yml up -d

services:
	@echo "Starting logstash and filebeat"
	docker compose -f docker-compose.services.yml up -d

setup:
	docker run --rm \
	-v $(cwd)/filebeat.iis.yml:/usr/share/filebeat/filebeat.yml \
	-e STACKWAVE_PROJECT=kibana-setup \
	--network=elastic \
	-t docker.elastic.co/beats/filebeat:7.16.2 \
	/bin/sh -c \
	"filebeat --strict.perms=false setup -e -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=[\"$(eshost)\"]';"

stop_all: stop_services stop_deps
	@echo "Stopping everything"

stop_deps:
	@echo "Stopping elasticsearch and kibana"
	docker compose -f docker-compose.deps.yml down

# this will fail removing the network, this is ok since `deps` is still using it
stop_services:
	@echo "Stopping logstash and filebeat"
	docker compose -f docker-compose.services.yml down || true 
